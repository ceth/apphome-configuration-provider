package de.cpotr.apphomeconfiguration;

/*   This file is part of apphome-configuration-provider plugin.
 *   Copyright &copy; 2013 Bastian Kraus, basti@cpotr.de  
 *     
 *   apphome-configuration-provider plugin is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.

 *   apphome-configuration-provider plugin is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with apphome-configuration-provider plugin.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Factory for creating new instances of {@link de.cpotr.apphomeconfiguration.ApphomeConfigurationProvider}
 * 
 * @author Bastian Kraus <basti@cpotr.de>
 */
public interface ApphomeConfigurationProviderFactory {

	/**
	 * Creates a new instance of {@link de.cpotr.apphomeconfiguration.ApphomeConfigurationProvider}
	 * 
	 * @return new instance of {@link de.cpotr.apphomeconfiguration.ApphomeConfigurationProvider}
	 */
	public ApphomeConfigurationProvider getInstance(String configFileName);
	
}