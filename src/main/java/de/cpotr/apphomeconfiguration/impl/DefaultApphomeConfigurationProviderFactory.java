package de.cpotr.apphomeconfiguration.impl;

/*   This file is part of apphome-configuration-provider plugin.
 *   Copyright &copy; 2013 Bastian Kraus, basti@cpotr.de  
 *     
 *   apphome-configuration-provider plugin is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.

 *   apphome-configuration-provider plugin is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with apphome-configuration-provider plugin.  If not, see <http://www.gnu.org/licenses/>.
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.config.bootstrap.AtlassianBootstrapManager;

import de.cpotr.apphomeconfiguration.ApphomeConfigurationProvider;
import de.cpotr.apphomeconfiguration.ApphomeConfigurationProviderFactory;

/**
 * See {@link de.cpotr.apphomeconfiguration.ApphomeConfigurationProviderFactory}
 * 
 * @author Bastian Kraus <basti@cpotr.de>
 */
public class DefaultApphomeConfigurationProviderFactory implements ApphomeConfigurationProviderFactory {

	private final static Logger log = LoggerFactory.getLogger(DefaultApphomeConfigurationProviderFactory.class);
	
	private AtlassianBootstrapManager bootstrapManager;
	
	public DefaultApphomeConfigurationProviderFactory(AtlassianBootstrapManager bootstrapManager) {
		log.debug("injecting bootstrap manager...");
		this.bootstrapManager = bootstrapManager;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public ApphomeConfigurationProvider getInstance(String configFileName) {
		try {
			DefaultApphomeConfigurationProvider instance = new DefaultApphomeConfigurationProvider(configFileName);
			instance.afterPropertiesSet();
			return (ApphomeConfigurationProvider) instance;
		} catch (Exception e) {
			log.error(e.toString());
			e.printStackTrace();
		}
		return null;
	}
	
}