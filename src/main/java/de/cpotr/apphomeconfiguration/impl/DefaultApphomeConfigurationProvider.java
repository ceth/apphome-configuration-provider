package de.cpotr.apphomeconfiguration.impl;

/*   This file is part of apphome-configuration-provider plugin.
 *   Copyright &copy; 2013 Bastian Kraus, basti@cpotr.de  
 *     
 *   apphome-configuration-provider plugin is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.

 *   apphome-configuration-provider plugin is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with apphome-configuration-provider plugin.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import com.atlassian.config.util.BootstrapUtils;

import de.cpotr.apphomeconfiguration.ApphomeConfigurationProvider;

/**
 * Implementation of {@link de.cpotr.apphomeconfiguration.PluginConfigurationProvider}
 * 
 * {@inheritDoc}
 * 
 * @author Bastian Kraus <basti@cpotr.de>
 */
public class DefaultApphomeConfigurationProvider implements ApphomeConfigurationProvider, InitializingBean {

	private final Logger log = LoggerFactory.getLogger(DefaultApphomeConfigurationProvider.class);

	private final String configFilePath;
	private final String configFileName;
	
	private Properties properties = null;
	
	public DefaultApphomeConfigurationProvider(String configFileName) {
		this.configFileName = configFileName;
		this.configFilePath = BootstrapUtils.getBootstrapManager().getApplicationHome() + System.getProperty("file.separator") + "config";
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		log.debug("... <LAZY> afterPropertiesSet() ...");
		log.debug(" ... config file path: " + configFilePath + System.getProperty("file.separator") + this.configFileName);
		
		try {
			log.debug("initialize plugin configuration properties ...");
			if(!initializePropertiesFromFile()) {
			//if(!initializePropertiesFromCode()) {
				throw new Exception("Unable to initialize properties from file: " + configFilePath + System.getProperty("file.separator") + this.configFileName);
			}
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new RuntimeException(e.getMessage(), e);
		}		
	}
	
	@Override
	public String getProperty(String name) {
		if(properties.containsKey(name)) return properties.getProperty(name);
		else return null;
	}
	
	@Override
	public Properties getProperties() {
		return this.properties;
	}

	/**
	 * Initialize the property file from <APP_HOME>/config/<CONFIG_FILE_NAME>
	 * @return true if initialization ok, false if not
	 */
	protected synchronized boolean initializePropertiesFromFile() {
		boolean success = false;
		
		File propertiesFile = new File(this.configFilePath, this.configFileName);
		properties = new Properties();
		try {
			if(!propertiesFile.exists())
				throw new FileNotFoundException("Properties file '" + this.configFilePath + System.getProperty("file.separator") + this.configFileName + "' does not exist!");
			if(!propertiesFile.canRead())
				throw new FileNotFoundException("Unable to read properties file: '" + this.configFilePath + System.getProperty("file.separator") + this.configFileName + "'");
			
            FileInputStream propertyInputStream = new FileInputStream(propertiesFile);
			properties.load(propertyInputStream);
			
			success = true;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			success = false;
		}		
		
		return success;
	}
	
}