package de.cpotr.apphomeconfiguration;

/*   This file is part of apphome-configuration-provider plugin.
 *   Copyright &copy; 2013 Bastian Kraus, basti@cpotr.de  
 *     
 *   apphome-configuration-provider plugin is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.

 *   apphome-configuration-provider plugin is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with apphome-configuration-provider plugin.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Properties;

/**
 * Der PluginConfigurationProvider laed Parameter aus der hart-codierten <artifactId>.properties Datei
 * aus <CONFLUENCE_HOME>/config/. Existiert diese Datei nicht, wird versucht sie anzulegen.
 * 
 * @author bkraus
 */
public interface ApphomeConfigurationProvider {

	/**
	 * Loads a Property specified by name param
	 * @param name
	 * @return String of property value or null if nothing found
	 */
	String getProperty(String name);
	
	/**
	 * Gets the internal Properties object
	 * 
	 * @return
	 */
	Properties getProperties();
	
}