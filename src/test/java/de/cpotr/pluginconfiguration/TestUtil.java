package de.cpotr.pluginconfiguration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;


public class TestUtil {
	
	public static String getTestContextBasePath() {
		return "src/test/java";
	}
	
	/**
	 * Laed eine Propertydatei aus dem per getTestContextBasePath() definierten Kontext.
	 * 
	 * @param fileName
	 * @return
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static Properties getTestProperties(String fileName) throws IOException, FileNotFoundException {
		
		File propertiesFile = new File(TestUtil.getTestContextBasePath() + System.getProperty("file.separator") + "config",fileName);
		Properties properties = new Properties();
		
		if(!propertiesFile.exists())
			throw new FileNotFoundException("Properties file '" + propertiesFile.getAbsolutePath() + "' does not exist!");
				
        FileInputStream propertyInputStream = new FileInputStream(propertiesFile);
        properties.load(propertyInputStream);
		
        return properties;
	}
	
}